import api from "./api";
import TokenService from "./token.service";

class AuthService {
  login({ email, password }) {
    return api
      .post("/auth/login", {
        email,
        password
      })
      .then(response => {
        if (response.data) {
          TokenService.setUser(response.data);
          TokenService.setToken(response.data.access_token);
        }

        return response.data;
      });
  }

  logout() {
    TokenService.removeUser();
  }

  forgot({ email }) {
    return api.post("/forgot-password/generate-code", {
      email
    });
  }

  register({ username, email, password }) {
    return api.post("/auth/signup", {
      username,
      email,
      password
    });
  }
}

export default new AuthService();
