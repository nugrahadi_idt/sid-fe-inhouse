export const token = {
  namespaced: true,
  state: {
    // user: null,
    token: null,
    refresh_token: null,
    parsed_token: null,
    parsed_refresh_token: null,
    is_superadmin: false
  },
  actions: {
    getAccessToken({ commit, state }) {
      return new Promise((resolve, reject) => {
        if (Date.now() >= state.parsed_token.exp * 1000) {
          // access token expired, get new access token if refresh token still valid
          if (Date.now() < state.parsed_refresh_token.exp * 1000) {
            // refresh token still valid
            const requestOptions = {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + state.refresh_token
              }
            };
            fetch(
              `${process.env.VUE_APP_URL_LOCAL}/token/refresh`,
              requestOptions
            )
              .then(r => r.json())
              .then(data => {
                commit("setToken", data.access_token);
                resolve(data.access_token);
              })
              .catch(reason => {
                reject(reason);
              });
          } else {
            // logout user
            this.$store
              .dispatch("auth/logout")
              .then(() => this.$router.push("/login"));
            reject("Refresh token expired, please re-login");
          }
        } else {
          resolve(state.token);
        }
      });
    }
  },
  mutations: {
    LOGIN_SUCCESS(state, response) {
      state.token = response.token;
    },
    setToken(state, payload) {
      state.token = payload;
    },
    setRefreshToken(state, payload) {
      state.refresh_token = payload;
    },
    setParsedToken(state, payload) {
      state.parsed_token = payload;
    },
    setParsedRefreshToken(state, payload) {
      state.parsed_refresh_token = payload;
    },
    setIsSuperAdmin(state, payload) {
      state.is_superadmin = payload;
    }
    /*setUser(state, user) {
      state.user = user;
    },
    setToken(state, token) {
      console.log(this.token);
      state.token = token;
    }*/
  }
};
