# Check out https://hub.docker.com/_/node to select a new base image
#FROM registry.spbe.sangkuriang.co.id/node:14.17.5-slim
FROM node:14-slim as build-stage

WORKDIR /app

COPY package*.json ./

RUN npm install
#RUN npm ci

COPY . .
#COPY env.dev .env
RUN npm run build

FROM bitnami/nginx:latest as production-stage
# RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /opt/bitnami/nginx/conf/nginx.conf

EXPOSE 3000